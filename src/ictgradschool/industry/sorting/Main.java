package ictgradschool.industry.sorting;

import ictgradschool.Keyboard;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Andrew Meads on 27/05/2017.
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        // Create random array of a desired size
        System.out.print("Size of array to sort: >");
        int len = Integer.parseInt(Keyboard.readInput());
        int[] array = new int[len];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        shuffleArray(array);
        System.out.println("Random array of size " + len + " created (containing all ints from 0 to " + (len - 1) + " in a random order).");

        System.out.println();

        // Create a desired ISortAlgorithm
        System.out.print("Please enter a sorting algorithm (just type the class name): >");
        String algName = Keyboard.readInput();
        Class<?> algClass = Class.forName("ictgradschool.industry.sorting." + algName);
        ISortAlgorithm algorithm = (ISortAlgorithm) algClass.newInstance();
        System.out.println(algClass.getName() + " created successfully.");

        System.out.println();

        // Sort
        System.out.print("Sorting array... ");
        long startTime = System.currentTimeMillis();
        algorithm.sort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("Done! Sorting took " + (endTime - startTime) + "ms.");

        System.out.println();

        // Verify sorting was successful
        System.out.print("Verifying sort... ");
        for (int i = 0; i < array.length; i++) {
            if (i != array[i]) {
                System.out.println("Error! Expected " + i + " at index " + i + " but was " + array[i] + ".");
                return;
            }
        }
        System.out.println("Sort was successful!");
    }

    /**
     * Shuffles an array.
     * @param array
     */
    private static void shuffleArray(int[] array) {
        int index, temp;
        Random random = ThreadLocalRandom.current();
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }
}
