package ictgradschool.industry.sorting;

import java.util.Arrays;

/**
 * Implementation of Insertion Sort to sort an array of integers.
 * <p>Uncomment the commented lines to view the intermediate output after each pass.</p>
 */
public class InsertionSort implements ISortAlgorithm {
    @Override
    public void sort(int[] numbers) {
//        int pass = 0;
        for(int i = 1; i < numbers.length; i++){
//            pass++;
//            int comp = 0;
//            int swap = 0;
            int position = i;
            while(position > 0){
                if(numbers[position - 1] > numbers[position]){
                    int temp = numbers[position];
                    numbers[position] = numbers[position - 1];
                    numbers[position - 1] = temp;
                    position--;
//                    comp++;
//                    swap++;
                }else{
//                    comp++;
                    break;
                }
            }
//            System.out.print(Arrays.toString(numbers));
//            System.out.println(" Pass " + pass + "(" + comp + " comp, " + swap + " swap)");
        }
    }
}
